public class AddUserToAccountTeam {
    
    public static void addUser(Id account,List<User> users,String AccessAccount,String CaseAccess,String OpportunityAccess)
    {
        List<AccountTeamMember> atmList = new List<AccountTeamMember>();
        for(User user:users)
        {
            AccountTeamMember atm = new AccountTeamMember();
            atm.accountid = account;
            atm.UserId = user.Id;
            atm.AccountAccessLevel = AccessAccount;
            atm.CaseAccessLevel = CaseAccess;
            atm.OpportunityAccessLevel = OpportunityAccess;
            atm.TeamMemberRole = 'Account Manager';
            atmList.add(atm);
        }
        if(atmList.size()>0)
            insert atmList;
    }
    public static void addUser(Id account,List<User> users)
    {
        List<AccountTeamMember> atmList = new List<AccountTeamMember>();
        for(User user:users)
        {
            AccountTeamMember atm = new AccountTeamMember();
            atm.accountid = account;
            atm.UserId = user.Id;
            atm.TeamMemberRole = 'Account Manager';
            atmList.add(atm);
        }
        if(atmList.size()>0)
            insert atmList;
    }
    public static void addUser(List<Account> accountList,User user)
    {
        List<AccountTeamMember> atmList = new List<AccountTeamMember>();
        for(Account acc:accountList)
        {
            AccountTeamMember atm = new AccountTeamMember();
            atm.accountid = acc.Id;
            atm.UserId = user.Id;
            atm.TeamMemberRole = 'Account Manager';
            atmList.add(atm);
        }
        if(atmList.size()>0)
            insert atmList;
    }
    public static void addUser(List<Account> accountList,User user,String AccessAccount,String CaseAccess,String OpportunityAccess)
    {
        List<AccountTeamMember> atmList = new List<AccountTeamMember>();
        for(Account acc:accountList)
        {
            AccountTeamMember atm = new AccountTeamMember();
            atm.accountid = acc.Id;
            atm.UserId = user.Id;
            atm.AccountAccessLevel = AccessAccount;
            atm.CaseAccessLevel = CaseAccess;
            atm.OpportunityAccessLevel = OpportunityAccess;
            atm.TeamMemberRole = 'Account Manager';
            atmList.add(atm);
        }
        if(atmList.size()>0)
            insert atmList;
    }

}